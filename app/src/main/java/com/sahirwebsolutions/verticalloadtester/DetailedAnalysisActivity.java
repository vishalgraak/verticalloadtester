package com.sahirwebsolutions.verticalloadtester;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Bhavya on 10-07-2016.
 */
public class DetailedAnalysisActivity extends AppCompatActivity {

    ListView listViewDetailedAnalysis;
    TextView textViewHighest2,textViewLowest2,textViewAverage2,textViewDate2;
    int ID,fileNumber=1;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.detailed_analysis_activity);

        textViewHighest2=(TextView) findViewById(R.id.textViewHighest2);
        textViewLowest2=(TextView) findViewById(R.id.textViewLowest2);
        textViewAverage2=(TextView) findViewById(R.id.textViewAverage2);
        textViewDate2=(TextView) findViewById(R.id.textViewDate2);


        dbHelper=new DBHelper(this);
        ID=getIntent().getIntExtra("id",0);

        textViewHighest2.setText(dbHelper.getHighest(ID));
        textViewLowest2.setText(dbHelper.getLeast(ID));
        textViewAverage2.setText(dbHelper.getAverage(ID));
        textViewDate2.setText(dbHelper.getDate(ID));



        listViewDetailedAnalysis =(ListView) findViewById(R.id.listViewDetailedAnalysis);
        CustomAdapter customAdapter=new CustomAdapter(this,dbHelper.getDataValues(ID),dbHelper.getTimeArrayList(ID));

        listViewDetailedAnalysis.setAdapter(customAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.menu_analysis,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.save_item){
           sendData(ID);
        }
        return true;
    }

    public void sendData(int id) {


        String filename = "Logs";
        if(isExternalStorageWritable()) {
            File logFile = new File("/sdcard/VerticalLoadTesterDirectory/", filename + fileNumber);

            while (logFile.exists()) {
                fileNumber++;

                logFile = new File("/sdcard/VerticalLoadTesterDirectory/", filename + fileNumber);
                //  Toast.makeText(AnalysisListActivity.this, "File exists", Toast.LENGTH_SHORT).show();

            }

            try {
                //FileOutputStream outputStream=new FileOutputStream(logFile,true);
                logFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


            saveLogs(dbHelper.getDate(id) + "  "+ dbHelper.getTime(id),logFile);
            saveLogs("-------------------------------------",logFile);

            saveLogs("Highest : " + dbHelper.getHighest(id),logFile);
            saveLogs("Least : " + dbHelper.getLeast(id),logFile);
            saveLogs("Average : " + dbHelper.getAverage(id),logFile);
            saveLogs("-------------------------------------",logFile);

            for(int i=0; i<dbHelper.getIds().size();i++){

                long totalTime= dbHelper.getTimeArrayList(id).get(i);
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                Date resultdate = new Date(totalTime);
                String timeString = sdf.format(resultdate);
                saveLogs(timeString+" : "+dbHelper.getDataValues(id).get(i),logFile);
            }

            Toast.makeText(this, "Logs saved at sdcard/VerticalLoadTesterDirectory/"+(filename+fileNumber)+".txt!", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "No external storage available!", Toast.LENGTH_SHORT).show();
        }
    }
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    private void saveLogs(String text,File logFile){
        if(isExternalStorageWritable()) {

            BufferedWriter buf = null;
            try {
                //BufferedWriter for performance, true to set append to file flag
                buf= new BufferedWriter(new FileWriter(logFile,true));
                buf.append(text);
                buf.newLine();

            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                if (buf != null)
                    try {
                        buf.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

        }
    }


    private class CustomAdapter extends BaseAdapter {

        private ArrayList<Double> dataAL;
        private ArrayList<Long> timeAL;
        private Context context;

        public CustomAdapter(Context context, ArrayList<Double> data,ArrayList<Long> timeList){

            this.context=context;
            dataAL=data;
            timeAL=timeList;
        }

        @Override
        public int getCount() {
            return dataAL.size();
        }

        @Override
        public Object getItem(int i) {
            return dataAL.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {

            if(view==null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view= inflater.inflate(R.layout.message2, null);
            }

            TextView textViewKg = (TextView) view.findViewById(R.id.textViewKg);
            TextView textViewNewton = (TextView) view.findViewById(R.id.textViewNewton);
            TextView textViewTime=(TextView) view.findViewById(R.id.textViewTimeListItem);
            long hours,minutes,seconds;
            long totalSecs=timeAL.get(i);
           /* hours = totalSecs / 3600;
            minutes = (totalSecs % 3600) / 60;
            seconds = totalSecs % 60;
*/

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date resultdate = new Date(totalSecs);
            String timeString = sdf.format(resultdate);
            //String timeString = String.format("%02d:%02d:%02d", hours, minutes, seconds);

            String kg=String.format("%.3f",dataAL.get(i));
            String newton=String.format("%.3f",dataAL.get(i)*9.8);
            textViewKg.setText(kg);
            textViewNewton.setText(newton);
            textViewTime.setText(timeString);


            return view;
        }
    }
}
